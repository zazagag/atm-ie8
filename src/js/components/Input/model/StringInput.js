import Input from './InputBase';
import InputChar from './InputChar';

/**
 * Модель представления поля ввода со строковым вводом.
 * @param {object} options - object settings
 * @constructor
 */
function StringInput(options) {
    this.options = options;
    this.init(true);
}

Input.prototype.extendBy(StringInput);

StringInput.prototype.pasteChar = function(character) {
    var newChar = new InputChar(character, {
        displayValue: this.options.isPwd ? this.options.pwdChar : character
    });

    // Вставить символ
    if (this.options.maxSymbolString > this.characters.length) {
        this.characters.splice(this.cursor.position, 0, newChar);
        this.cursor.position += 1;
        return true;
    }

    return false;

};

export default StringInput;