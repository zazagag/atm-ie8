﻿export default {
    /** Вставка */
    insert: 0,

    /** Замена */
    replace: 1,

    /** Замена всего */
    replaceAll: 2
};

/** Режимы курсора */