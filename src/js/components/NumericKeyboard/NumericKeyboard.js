﻿import React, {PropTypes} from 'react';
import cx from 'classnames';

import Keyboard from 'components/Keyboard';

import numLayouts from 'components/Keyboard/config/layouts/numeric';
import actions from 'components/Keyboard/config/actions';

import classes from './styles/NumericKeyboard.css';

export default class NumericKeyboard extends React.Component {

    static propTypes = {
        theme: PropTypes.oneOf(['dark', 'white']),
        keyboardEventEmitter: PropTypes.object.isRequired
    };

    static defaultProps = {
        theme: 'dark'
    };

    render() {
        return (
            <Keyboard className={cx(classes.root, {
                [classes['theme-dark']]: this.props.theme === 'dark',
                [classes['theme-white']]: this.props.theme === 'white'
            }, this.props.className)}
                config={{actions, layouts: numLayouts}}
                keyboardEventEmitter={this.props.keyboardEventEmitter} />
        );
    }
}