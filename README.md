#Simple app for IE8.

####Third-party libraries documentation

`Webpack` documentation: [webpack.github.io](https://webpack.github.io/)

`React` documentation: [facebook.github.io/react/docs/](https://facebook.github.io/react/docs/hello-world.html)

`Babel` documentation: [babeljs.io](https://babeljs.io)

####Nginx

Add [server section](./config/nginx.conf) to your nginx config.

Add `127.0.0.1 atm-ie8` to `hosts` file.

Restart nginx.

To start node server add real path to [forever config](./config/forever.config.json).

####JS
- [React@0.14.8](https://facebook.github.io/react/blog/2016/03/29/react-v0.14.8.html)
- ES6 import/export
- ES7 class properties
- ES7 decorators
- ES7 async/await
- ES7 generators
- Fetch([fetch-ie8](https://github.com/camsong/fetch-ie8))
- Promise([Bluebird](http://bluebirdjs.com/))
- ~~Minimizing mode(UglifyJs)~~

See [IndexPage.jsx](./src/js/pages/Index/IndexPage.jsx) for some examples.

####CSS

Built-in all defaults [cssnext](http://cssnext.io/features/) features.

####NPM main commands

|Script|Description|
|---|---|
|npm run init|Install application dependencies.|
|npm run prod|Compile and start app in `production*` mode.|

####NPM others commands

|Script|Description|
|---|---|
|npm run server|Compile server side sources for `production*` mode.|
|npm run client|Compile client side sources for `production*` mode.|
|npm run stylelint|Start `stylelint` checker.|
|npm run eslint|Start `eslint` checker.|
|npm run start|Start server in `production*` mode.|
|npm run stop|Stop all `forever` instances.|
|npm run test|Work in progress...|

*`production`* mode is mean the simply build without `devServer`, `sourceMap`.

####License

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)

[http://www.wtfpl.net/](http://www.wtfpl.net/) 

####Troubleshooting

*Work In Progress...*
