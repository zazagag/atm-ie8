import React, {Component, PropTypes} from 'react';

import Header from 'js/components/Header/Header';
import Menu from 'js/components/Menu/Menu';
import Footer from 'js/components/Footer/Footer';

import cls from './AppPage.css';

/**
 * AppPage
 */
class AppPage extends Component {
	static defaultProps = {
		children: null,
	};

	static propTypes = {
		children: PropTypes.object,
	};

	/**
	 * @return {XML}
	 */
	render() {
		return (
			<div className={cls['page__wrap']}>
                <Header />
                <Menu />
				<div className={cls['page']}>
					{this.props.children}
				</div>
				<Footer />
			</div>
		);
	}
}

export default AppPage;
