﻿import React, {PropTypes} from 'react';
import cx from 'classnames';
import Keyboard from '../Keyboard';
import actions from '../Keyboard/config/actions';
import fullLayout from './layouts/full';

import classes from './FullKeyboard.css';

export default class FullKeyboard extends React.Component {

    static propTypes = {
        keyboardEventEmitter: PropTypes.object.isRequired,
        theme: PropTypes.oneOf(['dark', 'light'])
    };

    static defaultProps = {
        theme: 'dark'
    };

    render() {
        let {theme, className, keyboardEventEmitter} = this.props;

        return (
            <Keyboard className={cx(
                classes.root, {
                    [classes['dark-theme']]: theme === 'dark',
                    [classes['light-theme']]: theme === 'light'
                }, className)}
                config={{actions, layouts: fullLayout}}
                startLayout={'en-normal'}
                keyboardEventEmitter={keyboardEventEmitter} />
        );
    }
}
