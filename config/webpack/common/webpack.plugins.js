const WebpackCleanupPlugin = require('webpack-cleanup-plugin');

const getPlugins = ({webpack, isServer, isProd, ExtractTextPluginInstance} = {}) => {
	const plugins = [
		new webpack.DefinePlugin({
			isServer: JSON.stringify(!!isServer),
			isProd: JSON.stringify(!!isProd),
			'process.env': {
				NODE_ENV: JSON.stringify(isProd ? 'production' : 'development'),
			},
		}),
//		new webpack.NoErrorsPlugin(),
		new WebpackCleanupPlugin({
			exclude: ['*.json', '.gitignore'],
		}),
	];

	if (isProd) {
		plugins.push(...[
			new webpack.optimize.OccurrenceOrderPlugin(),
		]);
	}

	if (ExtractTextPluginInstance) {
		plugins.push(...[
			ExtractTextPluginInstance,
		]);
	}

	return plugins;
};

module.exports = getPlugins;
