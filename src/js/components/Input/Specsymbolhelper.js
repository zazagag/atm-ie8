/**
 * helper for specSymbol
 * @param {string} e -  string from keyboards
 * @return {string} - string to model
 */
export default e => {
    let symbolBolToModel;
    switch (e) {
    case ' ':
        symbolBolToModel = '\u00A0';
        break;
    case '&nbsp;':
        symbolBolToModel = '\u00A0';
        break;
    case '&lt;':
        symbolBolToModel = '<';
        break;
    case '&gt;':
        symbolBolToModel = '>';
        break;
    case '&amp;':
        symbolBolToModel = '&';
        break;
    default:
        symbolBolToModel = e;
        break;
    }
    return symbolBolToModel;
};
