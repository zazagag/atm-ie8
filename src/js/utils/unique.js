let lastId = 0;

/**
 * Generate unique Id.
 * @param {string} prefix prefix
 * @return {string}
 */
export default (prefix = 'id') => {
	lastId += 1;
	return `${prefix}${lastId}`;
};
