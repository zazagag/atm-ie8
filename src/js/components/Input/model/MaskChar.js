﻿import maskCharTypes from './maskCharTypes';
/**
 * Модель представления символа маски.
 * @param {object} options - object settings
 */
export default function(options) {
    // Тип (буква, цифра, тд)
    this.type = maskCharTypes.any;

    // Отображаемое значение
    this.displayValue = undefined;

    // Редактируемый символ
    this.isEditable = false;

    // Символ обязательный к вводу
    this.isRequired = false;

    // Включать ли символ в результат
    this.isMandatory = false;
    Object.assign(this, options);

}
