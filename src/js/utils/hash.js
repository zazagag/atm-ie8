const paths = require('./../../../config/path.config.js');

const crypto = require('crypto');
const fs = require('fs');

const createHash = () => {
	const hash = crypto.createHash('md5').update(`${new Date()}`).digest('hex').slice(0, 6);
	fs.writeFileSync(paths.hash, JSON.stringify({id: hash}));
};

createHash();
