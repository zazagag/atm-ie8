import React, {Component} from 'react';

import styles from 'js/components/Menu/Menu.css';

/**
 * Menu
 */
class Menu extends Component {
	/**
	 * @return {XML}
	 */
	render() {
		return <div className={styles.menu}>
			<div className={styles.menu__item}>
				-= main =-
			</div>
			<div className={styles.menu__item}>
				-= about =-
			</div>
			<div className={styles.menu__item}>
				-= contact =-
			</div>
			<div className={styles.menu__item}>
				-= ads =-
			</div>
			<div className={styles.menu__item}>
				-= vacancy =-
			</div>
		</div>;
	}
}

export default Menu;
