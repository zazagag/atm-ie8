﻿import React, {PropTypes} from 'react';
import cx from 'classnames';

import Input from 'js/components/Input';

import classes from './styles/InputField.css';

export default class InputField extends React.Component {

    static propTypes = {
        options: PropTypes.object,
        theme: PropTypes.oneOf(['dark', 'white'])
    };

    static defaultProps = {
        theme: 'dark'
    };

    getValue() {
        return this.refs.input.getValue();
    }

    getState() {
        return this.refs.input.getState();
    }

    componentWillReceiveProps(nextProps) {
        // TBD: input currently does not support controlled behavior,
        //      only default value. So is we want to clear input without
        //      remounting - we should imperatively call clear.
        if (nextProps.options && !nextProps.options.value &&
            this.props.options && this.props.options.value) {

            this.refs.input.onClear();
        }
    }

    render() {
        let {className, options, theme, ...rest} = this.props;

        options = Object.assign({
            cursorEnabled: false,
            notUserInputClass: classes.ghost,
            notEditableClass: classes['not-editable'],
            replacements: {0: '_'}
        }, options);

        return (
            <Input className={cx(
                classes.root, {
                    [classes['theme-dark']]: theme === 'dark',
                    [classes['theme-white']]: theme === 'white'
                }, className)}
                   ref='input'
                   options={options}
                {...rest} />
        );
    }
}
