const paths = require('../../path.config.js');

module.exports = {
	root: [
		'node_modules',
		paths.src.root,
		paths.src.public,
		paths.src.config,
	],
	extensions: ['', '.js', '.jsx', '.less', '.json'],
};
