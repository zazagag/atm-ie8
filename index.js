const express = require('express');
const hash = require('./assets/hash.json').id;

const src = process.env.NODE_ENV === 'production' ? `./assets/server.${hash}` : './assets/server';
const page = require(src).page;
const api = require(src).api;

const app = express();
const port = 3333;

/**
 * Catch API requests.
 */
app.use('/api', api);
console.log('Api enable!');
process.on('unhandledRejection', (reason, promise) => {
	console.log('---[unhandledRejection]---');
	console.log(reason);
	console.log(promise);
});

/**
 * Catch common requests.
 */
app.use((req, res, next) => {
	page(req, res, next).then((options) => {
		res
			.status(options.status)
			.header({'Content-Type': 'text/html'})
			.end(options.html);
	}).catch((err) => {
		res
			.status(err.status)
			.end(err.html);
	});
});

/**
 * Start HTTP server.
 * @type {*|{remove}}
 */
app.listen(port, () => {
	console.log(`Listening on port: ${port}`);
});
