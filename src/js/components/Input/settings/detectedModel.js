import FractionInput from './../model/FractionInput';
import MaskedInput from './../model/MaskedInput';
import NumberInput from './../model/NumberInput';
import StringInput from './../model/StringInput';
import defaultSettings from './defaultsSettings';

/**
 * Detects model by current mode
 * @param {object} options - object with settings from react props
 * @return {object} model
 */
export default function(options) {
    let viewModel;
    options = Object.assign({}, defaultSettings.defaults, options);
    switch (options.mode) {
    case 'string':
        options = Object.assign({}, defaultSettings.defaultsString, options);
        viewModel = new StringInput(options);
        break;
    case 'fraction':
        options = Object.assign({}, defaultSettings.defaultsFraction, options);
        viewModel = new FractionInput(options);
        break;
    case 'mask':
        options = Object.assign({}, defaultSettings.defaultsMask, options);
        viewModel = new MaskedInput(options);
        break;
    case 'number':
        options = Object.assign({}, defaultSettings.defaultsNumber, options);
        viewModel = new NumberInput(options);
        break;
    default:
        throw new Error(`Unkown input mode '${options.mode}'`);
    }
    return viewModel;
}