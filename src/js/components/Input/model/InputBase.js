﻿import InputChar from './InputChar';
import cursorModes from './cursorModes';

/**
 * Модель представления поля ввода со свободным вводом.
 * @param {object} options - object settings
 * @constructor
 */
function Input(options) {
    this.options = options;
    this.init(true);
}
//#region Методы

/**
 * Инициализирует поле ввода.
 * @param {boolean} setValue - Выставлять ли дефолтное значение.
 */
Input.prototype.init = function(setValue) {
    this.characters = [];

    this.cursor = {
        position: 0,
        mode: cursorModes.insert
    };

    if (setValue && this.options.value) {
        // Поставить дефолтное значение
        this.paste(this.options.value);
    }
};

/**
 * Расширяет текущий класс.
 *
 */

Input.prototype.extendBy = function(child) {
    var f = function() {
    };
    f.prototype = Input.prototype;
    child.prototype = new f();
    child.prototype.constructor = child;
    child.superclass = Input.prototype;
};

/**
 * Возвращает полное текущее состояние модели (символы, выделение, сообщения валидации).
 */

Input.prototype.getState = function() {
    return {
        characters: this.characters,
        cursor: this.cursor,
        hasUserInput: this.checkUserInput(),
        validationMessages: [],
        isValid: true
    };
};

/**
 * Возвращает текущее значение поле ввода.
 * @returns {string}
 */

Input.prototype.getValue = function() {
    return this.characters.reduce(function(result, character) {
        return result + character.value;
    }, '');
};

/**
 * Проверяет содержит ли поле символы введённые пользователем.
 * @returns {boolean}
 */

Input.prototype.checkUserInput = function() {
    return this.characters.some(function(c) {
        return c.isUserInput;
    });
};

/**
 * Добавляет новые символы в поле ввода.
 * @param {string} text - Символ или последовательность символов.
 * @returns {boolean} Признак изменения текущего значения.
 */

Input.prototype.paste = function(text) {
    if (typeof text !== 'string') {
        throw Error('Invalid text pasted: ' + text);
    }

    var changed = false;

    if (this.cursor.mode === cursorModes.replaceAll) {
        changed = this.clear();
    }

    var chars = text.split('');

    chars.forEach(function(c) {
        changed = this.pasteChar(c);
    }.bind(this));

    return changed;
};

/**
 * Добавляет новый символ на текущую позицию курсора.
 * @param {string} character - Одиночный символ.
 * @return {boolean} Признак изменения текущего значения.
 */
Input.prototype.pasteChar = function(character) {
    var newChar = new InputChar(character, {
        displayValue: this.options.isPwd ? this.options.pwdChar : character
    });

    // Вставить символ
    this.characters.splice(this.cursor.position, 0, newChar);
    this.cursor.position += 1;

    return true;
};

/**
 * Удаляет один символ с текущей позиции курсора.
 * @return {boolean} Признак изменения текущего значения.
 */
Input.prototype.backspace = function() {
    var changed = false;

    if (this.cursor.mode === cursorModes.replaceAll) {
        changed = this.clear();
    }

    if (this.cursor.position !== 0) {
        // Удалить предыдущий символ
        this.characters.splice(this.cursor.position - 1, 1);
        this.cursor.position -= 1;
        changed = true;
    }

    return changed;
};

/**
 * Очищает поле ввода.
 * @return {boolean} Признак изменения текущего значения.
 */
Input.prototype.clear = function() {
    this.init(false);

    return true;
};

/**
 * Перемещает курсор.
 * @param {boolean} toRight - Вправо, иначе влево.
 */

Input.prototype.moveCursor = function(toRight) {
    if (this.cursor.mode === cursorModes.replaceAll) {
        // Перейти из режима замены всего значения целиком на режим вставки
        this.cursor.mode = cursorModes.insert;
        this.cursor.position = toRight ? this.characters.length : 0;
    }

    if (!toRight && this.cursor.position !== 0) {
        // Курсор влево
        this.cursor.position -= 1;
    }

    if (toRight && this.cursor.position !== this.characters.length) {
        // Курсор вправо
        this.cursor.position += 1;
    }
};

export default Input;