﻿import React, {PropTypes} from 'react';
import cx from 'classnames';
import Layout from './Keyboardlayout';
import fullLayouts from './config/layouts/full';
import actions from './config/actions';

import classes from 'js/components/Keyboard/Keyboard.css';

class Keyboard extends React.Component {

    static propTypes = {
        keyboardEventEmitter: PropTypes.object.isRequired,
        config: PropTypes.objectOf(PropTypes.object).isRequired,
        startLayout: PropTypes.string,
        className: PropTypes.string
    };

    static defaultProps = {
        keyboardEventEmitter: {
            emit: () => {}
        },
        config: {
            layouts: fullLayouts,
            actions
        }
    };

    constructor(props) {
        super(props);
        let firstLayout;
        if (props.startLayout) {
            firstLayout = props.startLayout;
        } else {
            firstLayout = Object.keys(props.config.layouts)[0];
        }
        this.state = {
            layout: firstLayout
        };
    }

    onActionClick = event => () => {
        this.props.config.actions[event[0]]
            .apply(this, event.slice(1, event.length));
    };

    onClick = symbol => () => {
        this.props.keyboardEventEmitter.emit(symbol);
    };

    render() {
        const {className, config} = this.props;
        return (
            <Layout
                layoutName={this.state.layout}
                layout={config.layouts[this.state.layout]}
                onChangeLayout={this.onActionClick}
                onClick={this.onClick}
                className={cx(classes.root, className) }
            />
        );
    }
}

export default Keyboard;
