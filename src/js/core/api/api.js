import express from 'express';
import bodyParser from 'body-parser';

const router = express.Router();
router.use(bodyParser.json());

router.post('/contact', (req, res) => {
	try {
		res.json({
			success: true,
			msg: 'Everything is alright!',
		});
	} catch (err) {
		res.json({
			success: false,
			error: err,
		});
	}
});

router.all('*', (req, res) => {
	res.json({error: 'unknown service!'});
});

export default router;
