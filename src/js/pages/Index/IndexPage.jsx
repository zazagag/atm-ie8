import React, {Component} from 'react';

import InputField from 'js/components/InputField/index';
import FullKeyboard from 'js/components/FullKeyboard/index';

import cls from './IndexPage.css';

/**
 * Simple decorator.
 * @param {number} id id
 * @return {function}
 */
function dec(id) {
	const data = {
		decoratorArgument: id,
		isBluebird: !!Promise.promisifyAll,
	};

	if (!isServer) {
		setTimeout(() => {
//			alert(`From decorator:\r\n${JSON.stringify(data)}`); // eslint-disable-line no-alert
		}, 0);
	}
	return (target, property, descriptor) => console.log('executed', id);
}

@dec(1)
/**
 * Index Page.
 * @constructor
 */
class IndexPage extends Component {
	instanceProperty = 'class property';

	boundFunction = () => {
		return this.instanceProperty;
	};

	static staticProperty = 'class static property';

	static staticFunction = () => {
		return IndexPage.staticProperty;
	};

	/**
	 * Fetch data for page.
	 * @return {Promise}
	 */
	static fetchData = async () => {
		const data = {
			staticProperty: IndexPage.staticProperty,
			staticFunction: IndexPage.staticFunction(),
		};
		return Promise.resolve(data);
	};

	keyboardEventEmitter = {
		emit: function() {}
	};

	/**
	 * @param {object} props props
	 * @constructor
	 */
	constructor(props) {
		super(props);

		this.onButtonClick = this.onButtonClick.bind(this);
	}

	/**
	 * Simple async func.
	 * @return {Promise}
	 */
	async waiter() {
		const data = await IndexPage.fetchData().then((res) => {
			return Object.assign(res, {
				instanceProperty: this.instanceProperty,
				boundFunction: this.boundFunction(),
			});
		});

		alert(`Data from fetchData:\r\n${JSON.stringify(data)}`); // eslint-disable-line no-alert

		return new Promise((resolve) => {
			fetch('/api/contact', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({request: 'init'}),
			}).then(res => res.json()).then((res = {}) => {
				resolve(res);
			});
		});
	}

	/**
	 * On button click cb.
	 * @return {void}
	 */
	async onButtonClick() {
		const fromWaiter = await this.waiter();
		alert(`Data from fetch API:\r\n${JSON.stringify(fromWaiter)}`); // eslint-disable-line no-alert
	}

	onInputChange = () => {
		console.log('input change');
	};

	/**
	 * @return {XML}
	 */
	render() {
		return (
			<div className="page__content page__content--index">
				IndexPage

				<div className={cls['form__wrap']}>
					<div className={cls['form']}>
						<InputField className={cls['form__input']}
									ref='input'
									theme={'white'}
									keyboardEventEmitter={this.keyboardEventEmitter}
									options={{
										value: '',
										cursorEnabled: true,
										mode: 'string',
										maxSymbolString: 160
									}}
									onChange={this.onInputChange} />

						<FullKeyboard
							className={'keyboard'}
							theme={'light'}
							keyboardEventEmitter={this.keyboardEventEmitter}
						/>

						<button className={cls['form__button']} onClick={this.onButtonClick}>submit</button>
					</div>
				</div>
			</div>
		);
	}
}

export default IndexPage;
