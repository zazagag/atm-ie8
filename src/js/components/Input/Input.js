import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import ReactDOMSelection from 'react/lib/ReactDOMSelection';
import ReactDOMServer from 'react/lib/ReactDOMServer';
import cursorModes from './model/cursorModes';
import detectModel from './settings/detectedModel';
import specSymbolsHelper from './Specsymbolhelper';
import cx from 'classnames';

import classes from './Input.css';

// TBD: Support controlled behavior.
// currently input is uncontrolled: you can set default value in props,
// but if for some reason you need to change value lately (e.g. clear input)
// without remounting - you forced to imperatively call clear, 
// insted of just changing props.
// I.e. native input supports both controlled and uncontrolled behavior.
// https://facebook.github.io/react/docs/forms.html#controlled-components
class Input extends React.Component {
    constructor() {
        super();
        this.viewModel = undefined;
        this.state = {
            symbol: undefined,
            idActiveInput: false
        };
    }

    static propTypes = {
        options: PropTypes.shape({
            userInputCss: PropTypes.object,
            notUserInputCss: PropTypes.object,
            notUserInputClass: PropTypes.string,
            notEditableClass: PropTypes.string
        }),

        /**
         * wait two objects with options this input and generator
         * with property keyboardEventEmitter.emit
         */
        keyboardEventEmitter: PropTypes.object.isRequired,
        onChange: PropTypes.func
    };

    static defaultProps = {
        onChange: () => {},
        options: {}
    };

    /*
     * draw placeholder
     * @return {object||boolean} {*} - object or false
     */
    ensurePlaceholder = vmState => {
        const classPlaceHolder = this.viewModel.options.placeholderClass;
        // Отрисовывать placeholder только когда нет пользовательского ввода
        // и есть placeholder, иначе должна быть показана маска
        if (!vmState.hasUserInput && this.viewModel.options.placeholder) {
            return ReactDOMServer.renderToStaticMarkup(
                <span key={1} contentEditable={false}
                      className={classes[classPlaceHolder]}>
                            {this.viewModel.options.placeholder}
                </span>);

        }
        // Отрисовывать placeholder с помощью класса css ::before,
        // только когда нет пользовательского ввода
        // и нет placeholder, иначе должна быть показана маска
        if (!vmState.hasUserInput && classPlaceHolder) {
            return ReactDOMServer.renderToStaticMarkup(
                <span key={1} contentEditable={false}
                      className={classes[classPlaceHolder]}>
                    {null}
                </span>);
        }
    };

    /** Отрисовывает текстовый курсор. */

    drawCursor = () => {
        if (this.viewModel.options.cursorEnabled) {
            let vmState = this.viewModel.getState();
            let offsetsCursor = {start: vmState.cursor.position};
            if (vmState.cursor.mode === cursorModes.replace) {
                offsetsCursor.end = offsetsCursor.start + 1;
            }
            ReactDOMSelection.setOffsets(
                ReactDOM.findDOMNode(this.refs.nameInput), offsetsCursor
            );
        }
    };

    /**
     * Обрабатывает событие ввода символа.
     * @param {jQuery.Event} e
     * @param {number} e.value.code - код символа.
     * @param {string} e.value.text - один или несколько символов.
     */

    onPaste(e) {
        switch (e) {
        case 8:
            return this.onBackspace();
        case 37:
            this.onCursorLeft();
            return true;
        case 39:
            this.onCursorRight();
            return true;
        case 27:
            // Escape
        case 46:
            // Delete
            this.onClear();
            return true;
        default:
            return false;

        }

    }

    getValue = () => this.viewModel.getValue();

    getState = () => this.viewModel.getState();

    /** Обрабатывает событие очистки поля ввода. */
    onClear = () => {
        this.viewModel.clear();
        this.onChange();
    };

    onBackspace = () => {
        if (this.viewModel.backspace()) {
            this.onChange();
            return true;
        }
        return false;
    };

    /** Обрабатывает событие перемещения курсора влево. */
    onCursorLeft = () => {
        if (!this.viewModel.options.cursorEnabled) {
            return;
        }
        this.viewModel.moveCursor(false);
    };

    /** Обрабатывает событие перемещения курсора вправо. */
    onCursorRight = () => {
        if (!this.viewModel.options.cursorEnabled) {
            return;
        }
        this.viewModel.moveCursor(true);
    };

    /**
     * Event handler for events from real and custom keyboard
     * @param {object|string} e - char or object event
     */
    onKey = e => {
        if (typeof e === 'object') {
            if (e.type === 'keypress') {
                //e - event keypress
                e.preventDefault();
                if (this.viewModel.pasteChar(
                        specSymbolsHelper(e.key.toString()))) {
                    this.onChange();
                }
                this.forceUpdate();

            } else if (this.onPaste(e.keyCode)) {
                this.forceUpdate();
            }
            // should prevent default behavior from real keyboard event
            // backspace and delete
            if (e.type === 'keydown') {
                if (e.keyCode === 8 || e.keyCode === 46) {
                    e.preventDefault();
                }
            }

        } else {
            // e - char element from jquery.kbrd
            if (this.viewModel.pasteChar(
                    specSymbolsHelper(e.toString()))) {
                this.onChange();
            }
            this.forceUpdate();

        }

    };

    onChange() {
        this.props.onChange(this.viewModel);
    }

    componentDidUpdate() {
        this.drawCursor();
    }

    componentWillMount() {
        const options = {
            ...this.props.options,
            userInputCss: this.props.options.userInputCss || {},
            notUserInputCss: this.props.options.notUserInputCss || {},
            notUserInputClass: this.props.options.notUserInputClass || '',
            notEditableClass: this.props.options.notEditableClass || ''
        };

        this.viewModel = detectModel(options);

        this.props.keyboardEventEmitter.emit = e => {
            this.onKey(e);
        };
    }

    /**
     * Create one char from model, and render to string
     * @param {object} charModel - object from model with description one symbol
     * @param {number}  keySpan - loop iterator
     * @param {string} notUserInputClass - string or undefined
     * @param {string} notEditableClass  - string or undefined
     * @return {*} - string
     */
    createCharElement = (charModel, keySpan,
        notUserInputClass, notEditableClass) => {
        let toRender =
            <span key={keySpan} style={charModel.isUserInput ?
                this.viewModel.options.userInputCss :
                this.viewModel.options.notUserInputCss}
                  className={cx({
                      [notEditableClass]: !charModel.isEditable,
                      [notUserInputClass]: !charModel.isUserInput
                  })}>
                        {charModel.displayValue}
            </span>;

        return ReactDOMServer.renderToStaticMarkup(toRender);
    };

    drawCharacters = vmState => {
        let notUserInputClass, notEditableClass;
        if (vmState.characters.length === 0) {
            return false;
        }
        if (this.viewModel.options.hasOwnProperty('notUserInputClass')) {
            notUserInputClass = this.viewModel.options.notUserInputClass;
        }
        if (this.viewModel.options.hasOwnProperty('notEditableClass')) {
            notEditableClass = this.viewModel.options.notEditableClass;
        }

        // Отрисовать каждый отдельный символ
        return vmState.characters.map((charModel, indx) => {
            return this.createCharElement(charModel, indx,
                notUserInputClass, notEditableClass);
        }).join('');
    };

    render() {
        const vmState = this.viewModel.getState();
        const toRender =
            this.ensurePlaceholder(vmState) ||
            this.drawCharacters(vmState) ||
            null;
        const cursorEnabled = this.viewModel.options.cursorEnabled;
        const contenteditableClass =
            (this.viewModel.options.cursorEnabled) ?
                cx(classes.input) :
                cx(classes.input, classes.contenteditableFalse);
        return (
            <div
                ref='nameInput'
                contentEditable={cursorEnabled}
                tabIndex={'0'}
                onKeyPress={this.onKey}
                onKeyDown={this.onKey}
                className={cx(contenteditableClass, this.props.className)}
                dangerouslySetInnerHTML={{__html: toRender}}/>);
    }
}
export default Input;