﻿import React, {PropTypes} from 'react';
import cx from 'classnames';

class Layout extends React.Component {

    static propTypes = {
        layoutName: PropTypes.string.isRequired,
        layout: PropTypes.array.isRequired,
        onChangeLayout: PropTypes.func.isRequired,
        onClick: PropTypes.func.isRequired,
        className: PropTypes.string
    };
    static defaultProps = {
        onChangeLayout: () => {},
        onClick: () => {}
    };

    parseAction = symbol => {
        let nameToShow, action;
        let actionString = symbol;

        if (symbol.includes(']')) {
            actionString = symbol.split(']');
            nameToShow = actionString.shift().substr(1);
        }

        action = actionString
            .toString().match(/^\{(\S+)\}$/)[1].split(':');

        return {
            action,
            name: nameToShow || action[0]
        };

    };

    buildRow(row, symbols) {
        const {onChangeLayout, layoutName, onClick} = this.props;

        return (
            <div className={`row${row + 1}`}
                 key={`row${row}`}>
                {symbols.map((symbol, index) => {
                    if (!/^\{\S+\}$/.test(symbol) &&
                        !/^\[\S+\}$/.test(symbol)) {
                        return (
                            <button onClick={onClick(symbol)}
                                    data-value={symbol}
                                    key={`${layoutName}${index}`}
                            >
                                {symbol}
                            </button>
                        );
                    }

                    const actionConfig = this.parseAction(symbol);

                    return (
                        <button data-value={actionConfig.action[0]}
                                onClick={onChangeLayout(actionConfig.action)}
                                key={`${layoutName}${index}`}
                        >
                            {actionConfig.name}
                        </button>
                    );

                })}</div>
        );
    }

    render() {
        const {layout, layoutName, className} = this.props;
        return (
            <div className={cx(className, `layout-${layoutName}`)}>
                {layout.map(function(symbols, row) {
                    let rowSymbols = symbols.split(' ');
                    return this.buildRow(row, rowSymbols);
                }, this)}
            </div>
        );
    }
}

export default Layout;
