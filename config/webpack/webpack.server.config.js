const webpack = require('webpack');
const pathConfig = require('./../path.config.js');
const getLoaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const hash = require(pathConfig.hash).id;
const ExtractTextPluginInstance = new ExtractTextPlugin(`../public/static/assets/app.${hash}.css`, {allChunks: true});

module.exports = {
	devtool: false,
	entry: {
		server: [
			'babel-polyfill',
			pathConfig.entry.server,
		],
	},
	target: 'node',
	output: {
		path: pathConfig.output.server,
		filename: `[name].${hash}.js`,
		libraryTarget: 'commonjs2',
		publicPath: '../assets/',
	},
	externals: /^[a-z\-0-9]+$/,
	plugins: getPlugins({
		webpack: webpack,
		isProd: true,
		isServer: true,
		ExtractTextPluginInstance: ExtractTextPluginInstance,
	}),
	module: {
		loaders: [
			...getLoaders(hash, true),
		],
	},
	resolve: resolve,
	postcss: () => {
		return [
			require('postcss-import')(),
			require('postcss-cssnext')(),
		];
	},
};
