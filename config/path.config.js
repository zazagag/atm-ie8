const path = require('path');

/**
 * @type {object}
 */
module.exports = {
	src: {
		/**
		 * @default '/src'
		 */
		root: path.resolve(__dirname, '../src'),

		/**
		 * @default '/src/js'
		 */
		js: path.resolve(__dirname, '../src/js'),

		/**
		 * @default '/src/less'
		 */
		css: path.resolve(__dirname, '../src/less'),

		/**
		 * @default '/public'
		 */
		public: path.resolve(__dirname, '../../public'),

		/**
		 * @default '/config'
		 */
		config: path.resolve(__dirname, '.'),

		/**
		 * @default '/node_modules'
		 */
		node_modules: path.resolve(__dirname, '../node_modules'),
	},

	entry: {
		client: path.resolve(__dirname, '../src/js/core/client/index.js'),
		server: path.resolve(__dirname, '../src/js/core/server/server.js'),
	},

	output: {
		server: path.resolve(__dirname, '../assets'),
		client: path.resolve(__dirname, '../public/static/assets'),
	},

	/**
	 * @default '/assets/hash.json'
	 */
	hash: path.resolve(__dirname, '../assets/hash.json'),

	/**
	 * @default '/static/assets/'
	 */
	public: '/static/assets/',
};
