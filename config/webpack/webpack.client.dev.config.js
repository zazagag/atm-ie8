const webpack = require('webpack');
const pathConfig = require('./../path.config.js');
const getLoaders = require('./common/webpack.loaders.js');
const resolve = require('./common/webpack.resolve.js');
const getPlugins = require('./common/webpack.plugins.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const hash = require(pathConfig.hash).id;
const ExtractTextPluginInstance = new ExtractTextPlugin(`[name].${hash}.css`, {allChunks: true});

module.exports = {
	devtool: 'source-map',
	entry: {
		app: [
			'console-polyfill',
			'babel-polyfill',
			'fetch-ie8',
			pathConfig.entry.client,
		],
	},
	output: {
		path: pathConfig.output.client,
		publicPath: pathConfig.public,
		filename: `[name].${hash}.js`,
	},
	plugins: [
		...getPlugins({
			webpack: webpack,
			isProd: true,
			ExtractTextPluginInstance: ExtractTextPluginInstance,
		}),
	],
	module: {
		loaders: [
			...getLoaders(hash),
		],
	},
	resolve: resolve,
	node: {
		fs: 'empty',
	},
	postcss: (webpack) => {
		return [
			require('postcss-import')(),
			require('postcss-cssnext')(),
		];
	},
};
