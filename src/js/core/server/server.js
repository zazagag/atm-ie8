import React from 'react';
import {renderToString} from 'react-dom/server';

import api from 'js/core/api/api';
import AppPage from 'js/pages/App/AppPage';
import IndexPage from 'js/pages/Index/IndexPage';

const hash = require('../../../../assets/hash').id;

/**
 * Enter point of application.
 * @param {object} req object
 * @param {object} res object
 * @param {function} next function
 * @return {Promise}
 */
const page = (req, res, next) => {
	return new Promise((resolve, reject) => {
		const cssAppLink = `/static/assets/app.${hash}.css`;
		const jsAppLink = `/static/assets/app.${hash}.js`;
		const app = renderToString(<AppPage><IndexPage /></AppPage>);
		const html = `<!DOCTYPE html>
					<html>
						<head>
							<meta charset="utf-8">
							<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
							<meta name="viewport" content="width=1024">
							<link rel="stylesheet" href="${cssAppLink}"/>
							<title>ATM IE8</title>
						</head>
						<body>
							<div class="root" id="root">${app}</div>
							<script type="text/javascript" src="${jsAppLink}"></script>
						</body>
					</html>`;

		resolve({html: html, status: 200});
	});
};

module.exports = {
	api: api,
	page: page,
};
