import React from 'react';

import cls from 'js/components/Header/Header.css';

/**
 * Header
 * @returns {XML}
 * @constructor
 */
const Header = () => {
	return (
		<div className={cls['header']}>
			<div className={cls['header__name']}>
				BasketLand<sup>®</sup>
			</div>
		</div>
	);
};

export default Header;
