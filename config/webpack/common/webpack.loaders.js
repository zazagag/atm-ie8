const ExtractTextPlugin = require('extract-text-webpack-plugin');

const getLoaders = (hash, isProd) => {
	const loaders = [{
		test: /\.js|\.jsx$/,
		exclude: /node_modules/,
		loaders: ['babel'],
	}, {
		test: /\.json$/,
		loader: 'json',
	}, {
		test: /\.css$/,
		loader: ExtractTextPlugin.extract('style-loader', [
			`css-loader?modules&importLoaders=1&localIdentName=[name]-[local]&${isProd ? 'minimize' : 'sourceMap'}`,
			'postcss-loader',
		]),
		exclude: /node_modules/,
	}, {
		test: /\.jpg|\.png$/,
		loader: `file-loader?name=images/[name].${hash}.[ext]`,
	}];

	return loaders;
};

module.exports = getLoaders;
