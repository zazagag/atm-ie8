﻿import InputChar from './InputChar';
import cursorModes from './cursorModes';
import Input from './InputBase';

/**
 * Модель представления поля ввода дробных чисел.
 * @param {object} options - object settings
 * @constructor
 */
function FractionInput(options) {
    this.options = options;
    this.init(true);
}

Input.prototype.extendBy(FractionInput);

//#region Публичные методы

/**
 * Инициализирует поле ввода.
 * @override
 * @param {boolean} setValue - Выставлять ли дефолтное значение.
 */

FractionInput.prototype.init = function(setValue) {
    this.characters = [];

    this.cursor = {
        position: 0,
        mode: cursorModes.replace
    };

    // Показать маску (напр. '0.00')
    this.characters.push(createDefaultChar.call(this));
    if (this.options.scale > 0) {
        this.pointerChar = new InputChar('.', {
            displayValue: this.options.pointer,
            isUserInput: false
        });

        this.characters.push(this.pointerChar);
        for (var i = 0; i < this.options.scale; i++) {
            this.characters.push(createDefaultChar.call(this));
        }
    }

    // Запомнить ограничения
    this.min = parseFloat(this.options.min);
    this.max = parseFloat(this.options.max);

    // Выставить дефолтное значение
    if (setValue && this.options.value) {
        this.paste(this.options.value);
    }
};

/**
 * Возвращает полное текущее состояние модели (символы, выделение, сообщения валидации).
 * @override
 */

FractionInput.prototype.getState = function() {
    var state = Input.prototype.getState.apply(this);

    // Подмешать данные валидации
    state.validationMessages = validate.call(this);
    state.isValid = isValid.call(this);

    return state;
};

/**
 * Возвращает текущее значение поле ввода.
 * @override
 * @returns {number}
 */

FractionInput.prototype.getValue = function() {
    var baseValue = Input.prototype.getValue.apply(this);
    return parseFloat(baseValue);
};

/**
 * Добавляет новый символ на текущую позицию курсора.
 * @param {string} character - Одиночный символ.
 * @override
 * @return {boolean} Признак изменения текущего значения.
 */

FractionInput.prototype.pasteChar = function(character) {
    if (!/\d/.test(character)) {
        // Обрабатывать все нечисловые символы как знак разделителя
        return pastePointer.call(this);
    }

    var newChar = new InputChar(character, {
        displayValue: this.options.isPwd ? this.options.pwdChar : character
    });

    var isIntegerPart = this.cursor.position < getPointerPos.call(this);

    switch (this.cursor.mode) {
    case cursorModes.insert:
        // Вставить символ
        this.characters.splice(this.cursor.position, 0, newChar);
        this.cursor.position += 1;
        break;

    case cursorModes.replace:
        // Заменить символ
        this.characters[this.cursor.position] = newChar;
        if (isIntegerPart ||
            (this.cursor.position < this.characters.length - 1)) {
            // Перейти на следующую позицию если идёт ввод целой части
            // и это не последний символ дробной части
            this.cursor.position += 1;
        }
        break;
    default:
        break;
    }

    onChanged.call(this);

    return true;
};

/**
 * Удаляет один символ с текущей позиции курсора.
 * @override
 * @return {boolean} Признак изменения текущего значения.
 */

FractionInput.prototype.backspace = function() {
    if (this.cursor.mode === cursorModes.replaceAll) {
        return this.clear();
    }

    switch (this.cursor.mode) {
    case cursorModes.insert:
        // Удалить предыдущий символ
        if (this.cursor.position !== 0) {
            this.characters.splice(this.cursor.position - 1, 1);
            this.cursor.position -= 1;
        }
        break;

    case cursorModes.replace:
        // Очистить текущий символ
        this.characters[this.cursor.position] = createDefaultChar.call(this);
        if (this.cursor.position !== 0) {
            this.cursor.position -= 1;
        }
        break;
    default:
        break;
    }

    onChanged.call(this);

    return true;
};

/**
 * Перемещает курсор.
 * @override
 * @param {boolean} toRight - Вправо, иначе влево.
 */
FractionInput.prototype.moveCursor = function(toRight) {
    if (this.cursor.mode === cursorModes.replaceAll) {
        // Перейти из режима замены всего значения целиком на режим ввода
        this.cursor.mode = cursorModes.insert;
        this.cursor.position = toRight ? this.characters.length - 1 : 0;
    }

    if (!toRight && this.cursor.position > 0) {
        // Курсор влево
        this.cursor.position -= 1;
    }

    if (toRight &&
        ((this.cursor.mode === cursorModes.replace &&
        (this.cursor.position < this.characters.length - 1)) ||
        (this.cursor.mode === cursorModes.insert &&
        (this.cursor.position < this.characters.length)))) {
        // Курсор вправо
        this.cursor.position += 1;
    }

    onChanged.call(this);
};

//#endregion

//#region Приватные методы

/**
 * Добавляет новый символ на текущую позицию курсора.
 * @return {boolean} Признак изменения текущего значения.
 */
function pastePointer() {
    if (this.pointerChar === undefined) {
        return false;
    }

    if (this.cursor.position <= getPointerPos.call(this)) {
        this.pointerChar.isUserInput = true;

        // Переместиться в дробную часть
        this.cursor.position = getPointerPos.call(this) + 1;

        onChanged.call(this, false);
    }

    return false;
}

/**
 * Создаёт новый символ маски.
 * @return {InputChar} - return object with one char
 */
function createDefaultChar() {
    return new InputChar('0', {
        displayValue: this.options.isPwd ? this.options.pwdChar : '0',
        isUserInput: false
    });
}

/**
 * Возвращает текущую позицию разделителя.
 * @return {number} - position in collection this.characters or length this.characters
 */
function getPointerPos() {
    return Boolean(this.pointerChar) ?
        this.characters.indexOf(this.pointerChar) :
        this.characters.length;
}

/**
 * Возвращает сообщения валидации.
 * @return {array} - array with strings
 */
function validate() {
    var messages = [];

    var value = this.getValue();

    if (value <= this.max) {
        messages.push('max_lost.input');
    }
    if (value < this.min) {
        messages.push('min_lost.input');
    }
    if (value >= this.min) {
        messages.push('min_exceeded.input');
    }
    if (value > this.max) {
        messages.push('max_exceeded.input');
    }

    return messages;
}

/**
 * Возвращает флаг валидности поля ввода.
 * @return {boolean} - boolean
 */
function isValid() {
    var value = this.getValue();

    return (value >= this.min) && (value <= this.max);
}

/**
 * Обрабатывает событие изменения значения.
 * @param {boolean} updatePointerState - boolean
 */
function onChanged(updatePointerState) {
    updatePointerState === undefined && (updatePointerState = true);

    if (Boolean(this.pointerChar) && updatePointerState) {
        // Обновить состояние разделителя
        // Введён - если в дробной части есть пользовательский ввод
        var fractionPart = this.characters.slice(getPointerPos.call(this) + 1,
            this.characters.length);
        this.pointerChar.isUserInput = fractionPart.some(function(c) {
            return c.isUserInput;
        });
    }

    // В целой части должен быть всегда хотя бы один символ
    var integerPart = this.characters.slice(0, getPointerPos.call(this));
    if (integerPart.length === 0) {
        var defaultChar = createDefaultChar.call(this);
        this.characters.splice(0, 0, defaultChar);
        integerPart.splice(0, 0, defaultChar);
    }

    // Не допускать лидирующий ноль в целой части
    if (integerPart[0].value === '0' && integerPart.length > 1) {
        this.characters.splice(0, 1);
        this.cursor.position -= 1;
    }

    // В случае перемещения курсора в конец поля и ввода символа
    // - все символы до него должны также считаться введёнными
    for (var i = this.characters.length - 1; i >= 0; i--) {
        if (this.characters[i].isUserInput) {
            var leftPart = this.characters.slice(0, i);
            leftPart.forEach(function(c) {
                c.isUserInput = true;
            });
            break;
        }
    }

    // Обновить режим курсора
    if (this.cursor.position <= getPointerPos.call(this)) {
        // Целая часть - режим ввода
        this.cursor.mode = cursorModes.insert;

        if (this.cursor.position === 0 && getPointerPos.call(this) === 1) {
            // Целая часть не введена - замена первого символа
            this.cursor.mode = cursorModes.replace;
        }
    } else {
        // Дробная часть - режим замены
        this.cursor.mode = cursorModes.replace;
    }
}

//#endregion

export default FractionInput;