﻿import Input from './InputBase';
import InputChar from './InputChar';
import MaskChar from './MaskChar';
import maskCharTypes from './maskCharTypes';
import cursorModes from './cursorModes';

/**
 * Модель представления поля ввода по маске.
 * @param {object} options - object settings
 * @constructor
 */
function MaskedInput(options) {
    this.options = options;
    this.init(true);
}

Input.prototype.extendBy(MaskedInput);

//#region Публичные методы

/**
 * Инициализирует поле ввода.
 * @override
 * @param {boolean} setValue - Выставлять ли дефолтное значение.
 *
 */

MaskedInput.prototype.init = function(setValue) {
    this.characters = [];

    this.cursor = {
        position: 0,
        mode: cursorModes.replace
    };

    this.isCursorInLastPosition = false;

    // Разобрать маску
    this.mask = parseMask.call(this,
        this.options.format,
        this.options.mask,
        this.options.mandatory,
        this.options.definitions,
        this.options.replacements);

    this.cursor.position = getNextEditablePos.call(this, -1, true);

    // define lastEditableIdx, use in mode cursorEnabled:false
    if (!this.options.cursorEnabled) {
        this.lastEditableIdx = getLastEditableCharIdx
            .call(this);
    }

    // Показать маску
    ensureMaskShown.call(this);

    ensureUserInput.call(this);

    // Запомнить ограничения
    this.requiredCharCount = createValidateRules.call(this);

    // Выставить дефолтное значение
    if (setValue && this.options.value) {
        this.paste(this.options.value);
    }

};

/**
 * Возвращает полное текущее состояние модели (символы, выделение, сообщения валидации).
 * @override
 */
MaskedInput.prototype.getState = function() {
    var state = Input.prototype.getState.apply(this);

    // Подмешать данные валидации
    state.isValid = isValid.call(this);

    for (let i = state.characters.length - 1; i >= 0; i--) {
        state.characters[i].isEditable = this.mask[i].isEditable;
    }

    return state;
};

/**
 * Возвращает текущее значение поле ввода.
 * @override
 * @returns {string}
 */

MaskedInput.prototype.getValue = function() {
    return {
        output: getOutput.call(this),
        outputForCheck: getOutputForCheck.call(this)
    };
};

/**
 * Проверяет содержит ли поле символы введённые пользователем.
 * @override
 * @returns {boolean}
 */

MaskedInput.prototype.checkUserInput = function() {
    return this.characters.some(function(c, idx) {
        return c.isUserInput && this.mask[idx].isEditable;
    }.bind(this));
};

/**
 * Добавляет новый символ на текущую позицию курсора.
 * @param {string} character - Одиночный символ.
 * @override
 * @returns {boolean} Признак изменения текущего значения.
 */

MaskedInput.prototype.pasteChar = function(character) {
    if (this.mask.length === 0 || this.cursor.position === -1) {
        return false;
    }

    // Проверить тип вводимого символа по маске
    var maskChar = this.mask[this.cursor.position];
    switch (maskChar.type) {
    case maskCharTypes.letter:
        if (!isLetter.call(this, character)) {
            return false;
        }
        break;
    case maskCharTypes.digit:
        if (!isDigit.call(this, character)) {
            return false;
        }
        break;
    default:
        break;
    }

    var newChar = new InputChar(character, {
        isUserInput: true,
        displayValue: this.options.isPwd ? this.options.pwdChar : character
    });

    if (!this.options.cursorEnabled) {
        if (this.isCursorInLastPosition) {
            return false;
        }

        if (this.cursor.position === this.lastEditableIdx) {
            this.isCursorInLastPosition = true;
        }
    }

    // Заменить символ
    this.characters.splice(this.cursor.position, 1, newChar);
    this.cursor.position = getNextEditablePos
        .call(this, this.cursor.position, true);

    ensureMaskShown.call(this);
    ensureUserInput.call(this);

    return true;
};

/**
 * Удаляет один символ с текущей позиции курсора.
 * @override
 * @returns {boolean} Признак изменения текущего значения.
 */

MaskedInput.prototype.backspace = function() {
    if (this.cursor.position === -1) {
        return false;
    }

    if (this.cursor.mode === cursorModes.replaceAll) {
        return this.clear();
    }

    // Заменить текущий символ на дефолтный символ маски и перейти влево
    var newChar = createDefaultChar.call(this, this.cursor.position);

    if (this.options.cursorEnabled) {
        // mode cursor enabled , casual mode 'insert'
        this.characters.splice(this.cursor.position, 1, newChar);
        this.cursor.position = getNextEditablePos
            .call(this, this.cursor.position, false);
    } else if (this.isCursorInLastPosition) {
        //mode cursor disabled, cursor stay in last position
        this.characters.splice(this.cursor.position, 1, newChar);
        this.isCursorInLastPosition = false;
    } else {
        //mode cursor disabled, cursor
        this.cursor.position = getNextEditablePos
            .call(this, this.cursor.position, false);
        // Mode cursorEnabled:false, override newChar after change cursor position
        newChar = createDefaultChar.call(this, this.cursor.position);
        this.characters.splice(this.cursor.position, 1, newChar);
    }

    ensureMaskShown.call(this);
    ensureUserInput.call(this);

    return true;
};

/**
 * Перемещает курсор.
 * @override
 * @param {boolean} toRight - Вправо, иначе влево.
 */
MaskedInput.prototype.moveCursor = function(toRight) {
    if (this.cursor.position === -1) {
        return;
    }

    if (this.cursor.mode === cursorModes.replaceAll) {
        // Перейти из режима замены всего значения целиком в режим замены
        this.cursor.mode = cursorModes.replace;
        // Сменить направление чтобы найти самый крайний левый,
        // либо крайний правый символ доступный для редактирования
        toRight = !toRight;
        this.cursor.position = toRight ? -1 : this.characters.length;
    }

    this.cursor.position = getNextEditablePos
        .call(this, this.cursor.position, toRight);

    ensureMaskShown.call(this);
};

//#endregion

//#region Приватные методы

/**
 * Разобрать маску.
 * @param {string} maskStr - Сама маска в виде строки.
 * @param {Object} changableGroupSyntax - Синтаксис группы заменяемых символов.
 * @param {Object} mandatoryGroupSyntax - Синтаксис группы символов обязательных к поступлению в строчный результат.
 * @param {Object} formatCharDef - Определения символов маски.
 * @param {Object} replacements - Словарь замены символов маски при отображении в поле ввода.
 * @return {Object} - Объект маски.
 */
function parseMask(maskStr, changableGroupSyntax, mandatoryGroupSyntax,
                   formatCharDef, replacements) {

    var mask = [];

    // Избавиться от escape символов
    [['\{', '{'], ['\}', '}']].forEach(function(r) {
        maskStr.replace(r[0], r[1]);
    });
    maskStr = maskStr.split('');

    // Разобрать каждый символ маски
    var isChangableGroup = false;
    var isMandatoryGroup = false;

    maskStr.forEach(function(c) {
        switch (c) {
            // Управляющие символы
        case changableGroupSyntax.start:
            isChangableGroup = true;
            break;
        case changableGroupSyntax.end:
            isChangableGroup = false;
            break;
        case mandatoryGroupSyntax.start:
            isMandatoryGroup = true;
            break;
        case mandatoryGroupSyntax.end:
            isMandatoryGroup = false;
            break;

            // Неуправляющие символы
        default:
            var maskChar = parseMaskChar.call(this,
                    c,
                    formatCharDef,
                    isChangableGroup,
                    isMandatoryGroup,
                    replacements);

            mask.push(maskChar);
        }
    }.bind(this));

    return mask;
}

/**
 * Разобрать отдельный символ маски.
 * @param {string} formatChar - Символ маски.
 * @param {Object} formatCharDef - Определения символов маски.
 * @param {boolean} isChangable - Изменяемый символ (доступный для ввода пользователя).
 * @param {boolean} isMandatory - Обязательно ли добавлять символ в результат.
 * @param {Object} replacements - Словарь замены символов маски при отображении в поле.
 * @return {Object}  - object
 */
function parseMaskChar(formatChar, formatCharDef, isChangable,
                       isMandatory, replacements) {
    var maskChar = new MaskChar();

    var displayValue;
    var charType;
    var isRequired;

    if (isChangable) {
        // Определить свойства заменяемого символа
        // (доступного для ввода пользователю)

        maskChar.isEditable = true;

        // Тип символов и обязательность ввода
        switch (formatChar) {
        case formatCharDef.chr:
            charType = maskCharTypes.letter;
            isRequired = true;
            break;
        case formatCharDef.chrOpt:
            charType = maskCharTypes.letter;
            isRequired = false;
            break;
        case formatCharDef.digit:
            charType = maskCharTypes.digit;
            isRequired = true;
            break;
        case formatCharDef.digitOpt:
            charType = maskCharTypes.digit;
            isRequired = false;
            break;
        case formatCharDef.any:
            charType = maskCharTypes.any;
            isRequired = true;
            break;
        case formatCharDef.anyOpt:
            charType = maskCharTypes.any;
            isRequired = false;
            break;
        default:
            charType = maskCharTypes.unknown;
            isRequired = false;
        }

        // Выводимое значение
        displayValue = formatChar;
        if (charType === maskCharTypes.unknown) {
            displayValue = replacements.unknown;
            maskChar.isEditable = false;
        } else if (replacements.hasOwnProperty(formatChar)) {
            displayValue = replacements[formatChar];
        }

        maskChar.type = charType;
        maskChar.displayValue = displayValue;
        maskChar.isRequired = isRequired;
        maskChar.isMandatory = true;
    } else {
        // Определить свойства незаменяемого символа
        // (статичные символы не доступные к изменению)
        maskChar.isEditable = false;
        maskChar.displayValue = formatChar;
        maskChar.isMandatory = isMandatory;
    }

    return maskChar;
}

/**
 * Возвращает следующую позицию маски доступную для пользовательского ввода.
 * @param {number} startPos Номер позиции. '-1' если редактируемых символов не найдено.
 * @param {number} right -  right
 * @return {*} - current position or startPos without change
 */
function getNextEditablePos(startPos, right) {
    var currentPos = right ? startPos + 1 : startPos - 1;
    while (currentPos >= 0 && currentPos < this.mask.length) {
        var currentChar = this.mask[currentPos];
        //!! - warning this was look like (!!currentChar && currentChar.isEditable)
        if (currentChar && currentChar.isEditable) {
            return currentPos;
        }

        right ? currentPos++ : currentPos--;
    }
    return startPos;
}

/**
 * Проверяет тип символа. Буква?
 * @param {string} c - единичный символ.
 * @return {boolean} - isLetter?
 */
function isLetter(c) {
    var code = c.charCodeAt(0);

    var result = false;

    this.options.allowedCharCodes.forEach(function(item) {
        if ((item.length === 1 && code === item[0]) ||
            (item.length === 2 && code >= item[0] && code <= item[1])) {
            result = true;
        }
        return result;
    });

    return result;
}

/**
 * Проверяет тип символа. Цифра?
 * @param {string} c - единичный символ.
 * @return {boolean} - result regular
 */
function isDigit(c) {
    return /\d/.test(c);
}

/** Строит символы маски. */
function ensureMaskShown() {

    var lastActiveCharPos = this.options.completeWithMask ?
    this.mask.length - 1 :
        getLastActiveCharPos.call(this);

    // Убедиться что маска собрана от начала
    // до последнего введённого символа или позиции курсора
    for (var i = 0; i <= lastActiveCharPos; i++) {
        if (this.characters[i] === undefined) {
            this.characters[i] = createDefaultChar.call(this, i);
        }
    }

    // Удалить всё что идёт дальше
    this.characters.splice(lastActiveCharPos + 1,
        this.characters.length - lastActiveCharPos);
}

/** Выставляет признак пользовательского ввода для символов. */
function ensureUserInput() {
    var lastInputChar = getLastActiveCharPos.call(this, false);

    // Все символы до последнего введённого пользователем
    // должны также считаться введёнными
    this.characters.slice(0, lastInputChar + 1)
        .forEach(function(c) {
            c.isUserInput = true;
        });

    this.characters.slice(lastInputChar + 1, this.characters.length)
        .forEach(function(c) {
            c.isUserInput = false;
        });
}

/**
 * Возвращает позицию крайнего правого отредактированного символа,
 * либо позицию курсора если она правее.
 * @param {boolean} considerCursor - boolean
 * @return {number} - last active position char
 */
function getLastActiveCharPos(considerCursor) {
    considerCursor === undefined && (considerCursor = true);

    var result = -1;
    if (this.characters.length > 0) {
        // Найти последний отредактированный символ
        for (var i = this.characters.length - 1; i >= 0; i--) {
            if (this.mask[i].isEditable && this.characters[i].isUserInput) {
                result = i;
                break;
            }
        }

        if (result === -1) {
            // Если редактированных символов нет,
            // то взять позицию перед первым редактируемым символом
            result = getFirstEditableCharPos.call(this) - 1;
        }

        var notEditableTail = this.mask.slice(result + 1, this.mask.length)
            .every(function(maskChar) {
                return !maskChar.isEditable;
            });

        if (notEditableTail) {
            // Если все символы после последнего введённого - не редактируемы,
            // то считать последней позицией крайний правый символ маски
            result = this.mask.length - 1;
        }
    }

    if (considerCursor) {
        result = Math.max(result, this.cursor.position);
    }

    return result;
}

/**
 * Возвращает позицию первого редактируемого символа.
 * @return {number} - return position
 */
function getFirstEditableCharPos() {
    var result = -1;
    const maskLength = this.mask.length;
    for (var i = 0; i < maskLength; i++) {
        if (this.mask[i].isEditable) {
            result = i;
            break;
        }
    }
    return result;
}

/**
 * Возвращает текущее значение поля ввиде строки.
 * @param {boolean} includeNotEditable - Включать ли нередактируемые символы маски в результат
 *                    (те что стоят левее введённых пользователем и также считаются введёнными).
 * @param {boolean} [includeMandatory=true] - Включать ли нередактируемые символы если они обязательные.
 * @return {string} - return string to show user
 */
function getInputString(includeNotEditable, includeMandatory) {
    includeMandatory === undefined && (includeMandatory = true);

    var userInputChars = this.characters.filter(function(c, idx) {
        return (includeNotEditable && this.characters[idx].isUserInput) ||
            (!includeNotEditable && this.characters[idx].isUserInput &&
            (this.mask[idx].isEditable ||
            (includeMandatory && this.mask[idx].isMandatory)));
    }.bind(this));

    // Преобразовать в строку
    return userInputChars.map(function(c) {
        return c.value;
    }).join('');
}

/**
 * Find last editable char in array this.mask
 * @return {number} - last editable char
 */
function getLastEditableCharIdx() {
    let lastInx = 0;
    for (let i = this.mask.length - 1; i >= 0; i--) {
        if (this.mask[i].isEditable) {
            lastInx = i;
            break;
        }
    }
    return lastInx;
}

/**
 * Создаёт дефолтный символ маски.
 * @param {number} maskPos - index array
 * @return {object} - object with one char
 */
function createDefaultChar(maskPos) {
    return new InputChar(
        this.mask[maskPos].displayValue,
        {
            isUserInput: false
        });
}

/**
 * generate rules to validate,
 * options.minStrLength or isRequired and isEditable symbols
 * @return { number } - count require symbols to validate
 */
function createValidateRules() {

    var requiredLength = 0;
    for (var i = this.mask.length - 1; i >= 0; i--) {
        if (this.mask[i].isEditable && this.mask[i].isRequired) {
            requiredLength++;
        }
    }
    return requiredLength;
}

/**
 * Возвращает значение поля ввода.
 * @return {Object}- object
 */
function getOutput() {
    var result = {};
    // Взять все введённые пользователем символы (в том числе символы маски)
    result[this.options.returnKey] = getInputString.call(this, true);

    return result;
}

/**
 * Возвращает значение поля ввода для чека.
 * @return {Object}- object
 */
function getOutputForCheck() {
    var result = {};

    result.checkonline = 1;

    // Взять все введённые пользователем символы (за исключением символов маски)
    result[this.options.returnKey] = getInputString.call(this, false);

    return result;
}

/**
 * Возвращает флаг валидности поля ввода.
 * @return {boolean} - isValid
 */
function isValid() {
    var value = getInputString.call(this, false);

    return value.length >= this.requiredCharCount;
}

//#endregion

export default MaskedInput;