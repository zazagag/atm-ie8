﻿
import Input from './InputBase';
import InputChar from './InputChar';
import cursorModes from './cursorModes';

/**
 * Модель представления поля ввода со свободным вводом.
 * @param {object} options - object settings
 * @constructor
 */
function NumberInput(options) {
    this.options = options;
    this.init(true);
}

Input.prototype.extendBy(NumberInput);

//#region Публичные методы

/**
 * Инициализирует поле ввода.
 * @param {boolean} setValue - Выставлять ли дефолтное значение.
 */

NumberInput.prototype.init = function(setValue) {
    this.characters = [];

    this.cursor = {
        position: this.options.capacity,
        mode: cursorModes.insert
    };

    // Показать маску (напр. '000')
    for (var i = 0; i < this.options.capacity; i++) {
        this.characters.push(createDefaultChar.call(this));
    }

    if (setValue && this.options.value) {
        // Поставить дефолтное значение
        this.paste(this.options.value);
    }
};

/**
 * Возвращает текущее значение поле ввода.
 * @returns {string}
 */

NumberInput.prototype.getValue = function() {
    var baseValue = Input.prototype.getValue.apply(this);
    return parseInt(baseValue, 10);
};

/**
 * Добавляет новый символ на текущую позицию курсора.
 * @param {string} character - Одиночный символ.
 * @returns {boolean} Признак изменения текущего значения.
 */

NumberInput.prototype.pasteChar = function(character) {
    if (!/\d/.test(character)) {
        // Игнорировать нечисловые символы
        return false;
    }

    var newChar = new InputChar(character, {
        displayValue: this.options.isPwd ? this.options.pwdChar : character
    });

    if (this.cursor.position === this.options.capacity) {
        // Курсор справа

        if (this.characters[0].isUserInput) {
            // Превышена вместимость
            return false;
        }

        // Выплюнуть символ маски слева, и добавить новый пользовательский справа
        this.characters.shift();
        this.characters.push(newChar);
    } else {
        // Курсор в режиме замены одного из символов

        // Заменить символ
        this.characters[this.cursor.position] = newChar;
    }

    return true;
};

/**
 * Удаляет один символ с текущей позиции курсора.
 * @returns {boolean} Признак изменения текущего значения.
 */

NumberInput.prototype.backspace = function() {
    var changed = false;

    if (this.cursor.mode === cursorModes.replaceAll) {
        changed = this.clear();
    }

    // Удалить крайний символ
    if (this.characters[this.options.capacity - 1].isUserInput) {
        this.characters.pop();
        this.characters.unshift(createDefaultChar.call(this));
        changed = true;
    }

    return changed;
};

/**
 * Перемещает курсор.
 * @param {boolean} toRight - Вправо, иначе влево.
 */

Input.prototype.moveCursor = function(toRight) {
    if (this.cursor.mode === cursorModes.replaceAll) {
        // Перейти из режима замены всего значения целиком на режим вставки
        this.cursor.mode = toRight ? cursorModes.insert : cursorModes.replace;
        this.cursor.position = toRight ? this.characters.length : 0;
    }

    if (!toRight && this.cursor.position !== 0 &&
        this.characters[this.cursor.position - 1].isUserInput) {
        // Курсор влево
        this.cursor.mode = cursorModes.replace;
        this.cursor.position -= 1;
    }

    if (toRight && this.cursor.position !== this.characters.length) {
        // Курсор вправо
        this.cursor.position += 1;

        if (this.cursor.position === this.characters.length) {
            this.cursor.mode = cursorModes.insert;
        } else {
            this.cursor.mode = cursorModes.replace;
        }
    }
};

//#endregion

//#region Приватные методы
/**
 * Создаёт новый символ маски.
 * @return {InputChar} - object with description one char
 */
function createDefaultChar() {
    return new InputChar('0', {
        displayValue: this.options.isPwd ? this.options.pwdChar : '0',
        isUserInput: false
    });
}

//#endregion

export default NumberInput;
