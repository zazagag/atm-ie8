﻿// Action key function list
const keyaction = {
    bksp: function() {
        this.props.keyboardEventEmitter.emit({
            keyCode: 8, preventDefault: () => {
            }
        });
    },
    space: function() {
        this.props.keyboardEventEmitter.emit('&nbsp;');
    },
    shift: function(param) {
        if (param) {
            this.setState({
                layout: param
            });
        } else {
            console.warn(`Target layout not specified for 'shift' action`);
        }

    },
    lang: function(param) {
        if (param) {
            this.setState({
                layout: param
            });
        } else {
            console.warn(`Target layout not specified for 'lang' action`);
        }
    },
    num: function() {

        if (this.state.layout === 'num') {
            this.setState({
                layout: this.state.prevlayout
            });
        } else {
            this.setState({
                prevlayout: this.state.layout,
                layout: 'num'
            });
        }

    }
};
export default keyaction;