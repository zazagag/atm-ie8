import React from 'react';
import {render} from 'react-dom';
import 'css/index.css';

import AppPage from 'js/pages/App/AppPage';
import IndexPage from 'js/pages/Index/IndexPage';

const root = document.querySelector('#root');

render(<AppPage>
	<IndexPage />
</AppPage>, root);
