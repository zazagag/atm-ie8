﻿/**
 * Модель представления отдельного символа поля ввода.
 * @param {string} value - symbol
 * @param {object} options - object settings
 */
export default function(value, options) {
    // Значение
    this.value = value;

    // Отображаемое значение
    this.displayValue = value;

    // Признак пользовательского ввода
    this.isUserInput = true;

    // Редактируемый символ
    this.isEditable = true;

    if (options) {
        Object.assign(this, options);
    }

    if (typeof this.value !== 'string' || this.value === '') {
        throw new Error('Invalid char value: ' + value);
    }

}